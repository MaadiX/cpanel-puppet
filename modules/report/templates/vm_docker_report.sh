#!/bin/bash

##List docker containers
echo "## Docker containers #######################################################"
docker container ls

##List docker images
echo "## Docker images ###########################################################"
docker images
